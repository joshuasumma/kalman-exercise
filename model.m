classdef model < handle
    %MODEL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access= public)
        angle = 45;
        alpha;
        v0 = 20;
        ay = -3.7;
        delta_t = 0.01;
        sigma_nu = 1; % measurement
        sigma_gamma = 0.01; % motion
        N = 760;
        p = [0, 0];
        vx0;
        vy0;
        px; py; vx; vy;
        px_gamma; py_gamma; vx_gamma; vy_gamma;
        px_nu; py_nu; vx_nu; vy_nu;
        kalman_state;
        export_k;
    end
    
    methods
        
        function obj = model()
            %MODEL Construct an instance of this class
            %   Detailed explanation goes here
            obj.alpha = pi * obj.angle / 180;
            obj.vx0 = sin(obj.alpha) * obj.v0;
            obj.vy0 = cos(obj.alpha) * obj.v0;
            obj.kalman_state = zeros(obj.N,4);
        end
        
        %% Apply Kalman in this case
        
        function [state, P] = kalman_filter(obj)
            
            A = [1 0  obj.delta_t 0;
                0 1        0 obj.delta_t;
                0 0        1 0;
                0 0        0 1];
            
            B = [0 0 0 0;
                 0 0 0 (1/2) * obj.delta_t;
                 0 0 0 0;
                 0 0 0 obj.delta_t];
            
             
            C = eye(4); %Measurement matrix 
            Gamma = eye(4) * obj.sigma_gamma ^2;
            Sigma = eye(4) * obj.sigma_nu ^2;
            V0 = eye(4)* obj.sigma_nu^2;
            
            %V0 = inv(C)*obj.sigma_nu^2*inv(C'); 
            
            state = [obj.p(1), obj.p(2), obj.vx0,  obj.vy0];
            signal = [obj.px_nu, obj.py_nu, obj.vx_nu, obj.vy_nu];
            u = [0,0,0,obj.ay];
            K_pre_diag = zeros(obj.N, 4);
            
            %Iteratie over each step
            [obj.kalman_state(1,:), P, K] = obj.kalman_filter_step(A, B, C, V0, Gamma, Sigma, state, signal(1, :), u);
            K_pre_diag(1,:) = [K(1,1), K(2,2), K(3,3), K(4,4)];
            for i = 2:obj.N
                [obj.kalman_state(i,:), P, K] = obj.kalman_filter_step(A, B, C, P, Gamma, Sigma, obj.kalman_state(i-1,:) , signal(i, :), u );
                K_pre_diag(i,:) = [K(1,1), K(2,2), K(3,3), K(4,4)];
            end
            
            obj.export_k = K_pre_diag;
            
        end
        
        
        
        %% Apply Kalman
        function [state, P, K] = kalman_filter_step(obj, A, B, C, P, Gamma, Sigma, state, signal  , u)
            %Dynamic Matrix A & B
            %Measurement Process Matrix C
            %measurement noise covariance matrix Sigma
            %initial noise covariance matrix V0
            %motion process noise covariance matrix Gamma
            %state vector
            %signal vector
            
            state = A * state' + B * u' ;
            % Prediction for state vector and covariance:
            P = A * P * A' + Gamma;
            
            % Compute Kalman gain factor:
            K = P * C' * inv( C * P * C' + Sigma);
            
            % Correction based on observation:
            state = state + K *(signal' - C * state);
            state = state';
            P = P - K*C*P;
        end
        %% Plot
        function plot_signals(obj, plot2)
            px_k = obj.kalman_state(:,1);
            py_k = obj.kalman_state(:,2);
            vx_k = obj.kalman_state(:,3);
            vy_k = obj.kalman_state(:,4);
            
            %% Kalman fehlt
            if(nargin < 2)
            f2 = figure;
            subplot(2,2,1);
            plot(obj.px); hold on; plot(obj.px_gamma); 
            plot(obj.px_nu);
            plot(px_k);
            title("Position in x over time");
            legend('Ground truth', 'Real, distureed positions', 'Measured Positions','Kalman Filter')
            
            subplot(2,2,2);
            plot(obj.py); hold on; plot(obj.py_gamma); 
            plot(obj.py_nu);
            plot(py_k);
            title("Position in y over time");
            legend;
            legend('Ground truth', 'Real, distureed positions', 'Measured Positions','Kalman Filter')
            
            subplot(2,2,3);
            plot(obj.vx, 'LineWidth', 2); hold on; plot(obj.vx_gamma, 'LineWidth', 0.2); 
            plot(obj.vx_nu, 'LineWidth' , 0.5); 
            hold on;
            plot(vx_k);
            title("Velocity in x over time");
            legend('Ground truth', 'Real, distureed positions', 'Measured Positions','Kalman Filter')
            
            subplot(2,2,4);
            plot(obj.vy); hold on; plot(obj.vy_gamma); 
            plot(obj.vy_nu); 
            hold on;
            plot(vy_k);
            title("Velocity in y over time");
            legend('Ground truth', 'Real, distureed positions', 'Measured Positions', 'Kalman Filter')
            end
            
            
            if(  nargin ==2  )
            f = figure;
            subplot(2,2,1);
            plot(obj.px, obj.py);
            title("Ground truth");
            subplot(2,2,2);
            plot(obj.px_gamma, obj.py_gamma);
            title("Real, distureed positions");
            subplot(2,2,3);
            plot(obj.px_nu, obj.py_nu);
            title("Measured Positions");
            subplot(2,2,4);
            plot(px_k, py_k);
            title("Kalman");
            hold on;
            end
            
        end
        
        %% Noise Application
        function out = add_noise(obj)
            %Gamma for position and velocity model of the ball
            obj.px_gamma = obj.add_defined_noise(obj.px, obj.sigma_gamma);
            obj.py_gamma = obj.add_defined_noise(obj.py, obj.sigma_gamma);
            obj.vx_gamma = obj.add_defined_noise(obj.vx, obj.sigma_gamma);
            obj.vy_gamma = obj.add_defined_noise(obj.vy, obj.sigma_gamma);
            %Nu for position and velocity measurements
            obj.px_nu = obj.add_defined_noise(obj.px_gamma, obj.sigma_nu);
            obj.py_nu = obj.add_defined_noise(obj.py_gamma, obj.sigma_nu);
            obj.vx_nu = obj.add_defined_noise(obj.vx_gamma, obj.sigma_nu);
            obj.vy_nu = obj.add_defined_noise(obj.vy_gamma, obj.sigma_nu);
            out ='null';
        end
        
        function signal_out = add_defined_noise(obj, signal_in, var)
            noise = sqrt(var).*randn(size(signal_in,1),1);
            signal_out = signal_in  + noise;
        end
        
        %% Run basic model
        function [px, py, vx, vy] = run(obj)
            px = zeros(obj.N,1);
            py = px; vx = px; vy = px;
            
            [px(1), py(1), vx(1), vy(1)] = obj.generateKinematicSteps( obj.ay, obj.delta_t, obj.p(1), obj.p(2), obj.vx0, obj.vy0);
            
            for i = 2:obj.N
                [px(i), py(i), vx(i), vy(i)] = obj.generateKinematicSteps(obj.ay, obj.delta_t, px(i-1), py(i-1), vx(i-1), vy(i-1));
            end
            
            obj.px = px;
            obj.py = py;
            obj.vx = vx;
            obj.vy = vy;
        end
        

        
    end
    
    methods (Access= public)
        function [pxk, pyk, vxk, vyk] = generateKinematicSteps(obj,ay, delta_t, pxk_1, pyk_1, vxk_1, vyk_1)
            pxk = pxk_1 + vxk_1 * delta_t;
            vxk = vxk_1;
            pyk = pyk_1 + vyk_1 * delta_t + (1/2) * ay * delta_t^2;
            vyk = vyk_1 + ay * delta_t;
        end
        
        function out = multivariate_normal_distribution(obj, dim,x, sigma)
            v = sqrt( (2 * pi)^dim * sigma );
            out = v *  exp( (-1/2) * x' * sigma^-1 * x); 
        end
    end
end

